/*
 * 
 */
package edu.westga.cs1302.bugcensus.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import edu.westga.cs1302.bugcensus.model.interfaces.Sortable;
import edu.westga.cs1302.bugcensus.resources.UI;

/**
 * The Class BugData.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class BugCensus implements Collection<Bug>, Sortable<Bug> {

	private int year;
	private ArrayList<Bug> bugs;

	/**
	 * Instantiates a new bug census.
	 *
	 * @precondition none
	 * @postcondition getBugs.size() == 0 && getNumberInsects() == 0 &&
	 *                getNUmberMyriapods() == 0
	 */
	public BugCensus() {
		Calendar now = Calendar.getInstance();
		this.year = now.get(Calendar.YEAR);
		this.bugs = new ArrayList<Bug>();
	}

	/**
	 * Instantiates a new bug census.
	 *
	 * @precondition year >= 0
	 * @postcondition getYear() == year && getBugs.size() == 0 && getNumberInsects()
	 *                == 0 && getNUmberMyriapods() == 0
	 * 
	 * @param year
	 *            the year
	 */
	public BugCensus(int year) {
		if (year < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_YEAR);
		}
		this.year = year;
		this.bugs = new ArrayList<Bug>();
	}

	/**
	 * Gets the year.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the year
	 */
	public int getYear() {
		return this.year;
	}

	/**
	 * Sets the year.
	 *
	 * @precondition year >= 0
	 * @postcondition getYear() == year
	 * 
	 * @param year
	 *            the new year
	 */
	public void setYear(int year) {
		if (year < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_YEAR);
		}
		this.year = year;
	}

	/**
	 * returns the size.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the size
	 */
	public int size() {
		return this.bugs.size();
	}

	/**
	 * Adds the bug.
	 *
	 * @precondition bug != null
	 * @postcondition size() == size()@prev + 1
	 * 
	 * @param bug
	 *            the bug
	 * @return true, if successful
	 */
	@Override
	public boolean add(Bug bug) {
		if (bug == null) {
			throw new NullPointerException(UI.ExceptionMessages.NULL_BUG);
		}

		return this.bugs.add(bug);
	}

	/**
	 * Gets the number insects.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the number insects
	 */
	public int getNumberInsects() {
		int numberInsects = 0;

		for (Bug currBug : this.bugs) {
			if (currBug instanceof Insect) {
				numberInsects++;
			}
		}

		return numberInsects;
	}

	/**
	 * Gets the number myriapodas.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number myriapodas
	 */
	public int getNumberMyriapodas() {
		int numberMyriapodas = 0;

		for (Bug currBug : this.bugs) {
			if (currBug instanceof Myriapoda) {
				numberMyriapodas++;
			}
		}

		return numberMyriapodas;
	}

	/**
	 * Gets the summary report.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the summary report
	 */
	public String getSummaryReport() {
		String report = "Bug Census of " + year;
		report += System.lineSeparator();
		report += "Total number bugs: " + this.size();
		report += System.lineSeparator();
		report += "Number insects: " + this.getNumberInsects();
		report += System.lineSeparator();
		report += "Total myriapodias: " + this.getNumberMyriapodas();
		return report;
	}

	@Override
	public boolean addAll(Collection<? extends Bug> bugs) {
		if (bugs == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_COLLECTION);
		}
		if (bugs.contains(null)) {
			throw new NullPointerException(UI.ExceptionMessages.NULL_IN_COLLECTION);
		}
		return this.bugs.addAll(bugs);
	}

	@Override
	public void clear() {
		this.bugs.clear();

	}

	@Override
	public boolean contains(Object bug) {
		return this.bugs.contains(bug);
	}

	@Override
	public boolean containsAll(Collection<?> bugs) {
		return this.bugs.containsAll(bugs);
	}

	@Override
	public boolean isEmpty() {
		return this.bugs.isEmpty();
	}

	@Override
	public Iterator<Bug> iterator() {
		return this.bugs.iterator();
	}

	@Override
	public boolean remove(Object bug) {
		if (bug == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BUG);
		}
		return this.bugs.remove(bug);
	}

	@Override
	public boolean removeAll(Collection<?> bugs) {
		if (bugs == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_COLLECTION);
		}
		if (bugs.contains(null)) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_IN_COLLECTION);
		}
		return this.bugs.removeAll(bugs);
	}

	@Override
	public boolean retainAll(Collection<?> bugs) {
		if (bugs == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_COLLECTION);
		}

		return false;
	}

	@Override
	public Object[] toArray() {
		return this.bugs.toArray();
	}

	@Override
	public <T> T[] toArray(T[] bug) {
		return this.bugs.toArray(bug);
	}

	@Override
	public void sort() {
		Collections.sort(this.bugs, (bug1, bug2) -> {
			return (int) (bug1.getLength() - bug2.getLength());
		});

	}

	@Override
	public void sort(Comparator<Bug> comp) {
		Collections.sort(this.bugs, comp);

	}
}
