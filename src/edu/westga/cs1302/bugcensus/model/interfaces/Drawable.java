package edu.westga.cs1302.bugcensus.model.interfaces;

import javafx.scene.canvas.Canvas;

/**
 * The Interface Drawable.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public interface Drawable {

	/**
	 * Gets the drawing.
	 *
	 * @return the drawing
	 */
	Canvas getDrawing();
	
	/**
	 * Gets the drawing set at the specified location.
	 *
	 * @param xCoor the x coordinate
	 * @param yCoor the y coordinate
	 * @return the drawing
	 */
	Canvas getDrawing(double xCoor, double yCoor);
	
	/**
	 * Gets the width of the drawing
	 *
	 * @return the width of the drawing
	 */
	double getDrawingWidth();
	
	/**
	 * Gets the height of the drawing
	 *
	 * @return the height of the drawing
	 */
	double getDrawingHeight();
}
